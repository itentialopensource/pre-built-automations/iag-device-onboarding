// BACKUP

import { WorkflowRunner, PrebuiltRunner } from '@itential-tools/iap-cypress-testing-library/testRunner/testRunners';


function initializeWorkflowRunner(workflow, importWorkflow, isStub, stubTasks) {
  let workflowRunner = new WorkflowRunner(workflow.name);
  let getDevicesWorkflowRunner = new WorkflowRunner(workflow.name);

  if (importWorkflow) {
    // cancel all running jobs for workflow
    workflowRunner.job.cancelAllJobs();
    getDevicesWorkflowRunner.job.cancelAllJobs();

    workflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false
    });

    // Check if Stub flag is enabled
    if (isStub) {
      stubTasks.forEach(stubTask => {
        workflow = workflowRunner.stub.task({
          stub: stubTask,
          workflow: workflow,
        });
      })
    }

    workflowRunner.importWorkflow.single({
      workflow,
      failOnStatusCode: false
    });
  }

  /* Verify workflow */
  workflowRunner.verifyWorkflow.exists();
  workflowRunner.verifyWorkflow.hasNoDuplicates({});
  // workflowRunner.verifyWorkflow.dependenciesOnline();

  return workflowRunner;
}



describe('Test Using Onboard Devices Main Workflow', function () {
  let onboardDevicesWorkflow;
  let getDevicesWorkflow;
  let onboardDeviceWorkflow;
  let ansibleDeviceOnboardingWorkflow;
  let inventoryDeviceOnboardingWorkflow;

  let getDeviceAnsibleStub;
  let getDeviceHTTPStub;
  let ansibleOnboardingIAGGDDGetDeviceDetailsChildJobStub;
  let ansibleOnboardingIAGDOAnsibleDeviceStub;
  let httpOnboardingIAGGDDGetDeviceDetailsChildJobStub;
  let httpOnboardingCreateInventoryDeviceStub;

  before(function () {
    // Import Workflows
    cy.fixture('../../../bundles/workflows/IAGDO: Onboard Devices.json').then((data) => {
      onboardDevicesWorkflow = data;
    });
    cy.fixture('../../../bundles/workflows/IAGGDD: Get Device Details.json').then((data) => {
      getDevicesWorkflow = data;
    });
    cy.fixture('../../../bundles/workflows/IAGDO: Onboard Device.json').then((data) => {
      onboardDeviceWorkflow = data;
    });
    cy.fixture('../../../bundles/workflows/IAGDO: Ansible Device Onboarding').then((data) => {
      ansibleDeviceOnboardingWorkflow = data;
    });
    cy.fixture('../../../bundles/workflows/IAGDO: Inventory Device Onboarding.json').then((data) => {
      inventoryDeviceOnboardingWorkflow = data;
    });

    /* Import Stubs */
    cy.fixture('stubs/getDeviceAnsibleStub.json').then((data) => {
      getDeviceAnsibleStub = data;
    });
    cy.fixture('stubs/getDeviceHTTPStub.json').then((data) => {
      getDeviceHTTPStub = data;
    });
    cy.fixture('stubs/ansibleOnboardingIAGGDDGetDeviceDetailsChildJobStub.json').then((data) => {
      ansibleOnboardingIAGGDDGetDeviceDetailsChildJobStub = data;
    });
    cy.fixture('stubs/ansibleOnboardingIAGDOAnsibleDeviceStub.json').then((data) => {
      ansibleOnboardingIAGDOAnsibleDeviceStub = data;
    });
    cy.fixture('stubs/httpOnboardingIAGGDDGetDeviceDetailsChildJobStub.json').then((data) => {
      httpOnboardingIAGGDDGetDeviceDetailsChildJobStub = data;
    });
    cy.fixture('stubs/httpOnboardingCreateInventoryDeviceStub.json').then((data) => {
      httpOnboardingCreateInventoryDeviceStub = data;
    });
    cy.fixture(
    '../../../artifact.json'
    ).then((data) => {
      /* Using prebuilt runner to ensure prebuilt exists in the IAP environment, this is done within the before all hook */
      let prebuiltRunner = new PrebuiltRunner(data);

      /* Delete existing prebuilt, if any, then reimport */
      prebuiltRunner.deletePrebuilt.single({ failOnStatusCode: false });
      prebuiltRunner.importPrebuilt.single({});
      prebuiltRunner.verifyPrebuilt.exists();
    });
  });

  // 1. Should successfully onboard ansible device.
  it("1. Should successfully onboard ansible device.", function () {
    const importOnboardDevicesWorkflowWorkflow = false;
    const onboardDevicesWorkflowWorkflowStub = false;
    const workflowRunner = initializeWorkflowRunner(onboardDevicesWorkflow, importOnboardDevicesWorkflowWorkflow, onboardDevicesWorkflowWorkflowStub, []);
    // Onboard Device Workflow Stub
    const importOnboardDeviceWorkflow = true;
    const onboardDeviceWorkflowStub = true;
    const onboardDeviceWorkflowRunner = initializeWorkflowRunner(onboardDeviceWorkflow, importOnboardDeviceWorkflow, onboardDeviceWorkflowStub, ansibleOnboardingIAGGDDGetDeviceDetailsChildJobStub.stubTasks);
    // Ansible Device Onboarding Workflow Stub
    const importAnsibleDeviceOnboardingWorkflow = true;
    const ansibleDeviceOnboardingWorkflowStub = true;
    const ansibleDeviceOnboardingWorkflowRunner = initializeWorkflowRunner(ansibleDeviceOnboardingWorkflow, importAnsibleDeviceOnboardingWorkflow, ansibleDeviceOnboardingWorkflowStub, ansibleOnboardingIAGDOAnsibleDeviceStub.stubTasks);

    const input = {
      "variables": {
        "device": [
          {
            "device_name": "PBLT_645_ansible_Test",
            "inventory_type": "ansible",
            "iag_adapter_instance": "DSUP IAG-2021.2",
            "onboarding": {
              "ipaddress": "172.20.103.91",
              "port": "22",
              "user": "admin",
              "password": "admin",
              "device-type": "network_cli",
              "ostype": "ios",
              "become": "true",
              "become_method": "enable",
              "become_pass": "admin"
            }
          }
        ],
        "options": {
          "verbose": false
        }
      },
      "description": "1. Should successfully onboard ansible device."
    };

    workflowRunner.job.startAndReturnResultsWhenComplete({
      options: input,
      retryTime: 2000,
    }).then((jobVariableResults) => {

      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['message']).to.eql('Successfully onboarded device to IAG');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['status']).to.eql('SUCCESS');

      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_successfully_onboarded_devices']).to.eql(1);
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_success']).to.eql(1);
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_failure']).to.eql(0);
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['message']).to.eql('0 device(s) found already onboarded to IAG, 1 device(s) successfully onboarded to IAG, 0 device(s) failed to onboard to IAG');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['status']).to.eql('SUCCESS');
    });

    // Reimport orginal Onboard Device Workflow
    onboardDeviceWorkflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false,
    });
    onboardDeviceWorkflowRunner.importWorkflow.single({ workflow: onboardDeviceWorkflow });
    onboardDeviceWorkflowRunner.verifyWorkflow.exists();
    onboardDeviceWorkflowRunner.verifyWorkflow.hasNoDuplicates({});

    // Reimport orginal Ansible Device Onboarding Workflow
    ansibleDeviceOnboardingWorkflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false,
    });
    ansibleDeviceOnboardingWorkflowRunner.importWorkflow.single({ workflow: ansibleDeviceOnboardingWorkflow });
    ansibleDeviceOnboardingWorkflowRunner.verifyWorkflow.exists();
    ansibleDeviceOnboardingWorkflowRunner.verifyWorkflow.hasNoDuplicates({});
  });

  // 2. Should fail onboarding existing ansible device.
  // it("2. Should fail onboarding existing ansible device.", function () {

  //   const importOnboardDevicesWorkflowWorkflow = false;
  //   const onboardDevicesWorkflowWorkflowStub = false;
  //   const workflowRunner = initializeWorkflowRunner(onboardDevicesWorkflow, importOnboardDevicesWorkflowWorkflow, onboardDevicesWorkflowWorkflowStub, []);
  //   const importgetDevicesWorkflow = true;
  //   const getDevicesWorkflowStub = true;
  //   const getDevicesWorkflowRunner = initializeWorkflowRunner(getDevicesWorkflow, importgetDevicesWorkflow, getDevicesWorkflowStub, getDeviceAnsibleStub.stubTasks);

  //   const input = {
  //     "variables": {
  //       "device": [
  //         {
  //           "device_name": "PBLT_645_ansible_Test",
  //           "inventory_type": "ansible",
  //           "iag_adapter_instance": "DSUP IAG-2021.2",
  //           "onboarding": {
  //             "ipaddress": "172.20.103.91",
  //             "port": "22",
  //             "user": "admin",
  //             "password": "admin",
  //             "device-type": "network_cli",
  //             "ostype": "ios",
  //             "become": "true",
  //             "become_method": "enable",
  //             "become_pass": "admin"
  //           }
  //         }
  //       ],
  //       "options": {
  //         "verbose": false
  //       }
  //     },
  //     "description": "2. Should fail onboarding existing ansible device."
  //   };

  //   workflowRunner.job.startAndReturnResultsWhenComplete({
  //     options: input,
  //     retryTime: 2000,
  //   }).then((jobVariableResults) => {

  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['message']).to.eql('Device already exists on IAG');
  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['status']).to.eql('SUCCESS');

  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_already_onboarded_devices']).to.eql(1);
  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_success']).to.eql(1);
  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_failure']).to.eql(0);
  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['message']).to.eql('1 device(s) found already onboarded to IAG, 0 device(s) successfully onboarded to IAG, 0 device(s) failed to onboard to IAG');
  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['status']).to.eql('SUCCESS');
  //   });
  //   getDevicesWorkflowRunner.deleteWorkflow.allCopies({
  //     failOnStatusCode: false,
  //   });
  //   getDevicesWorkflowRunner.importWorkflow.single({ workflow: getDevicesWorkflow });
  //   getDevicesWorkflowRunner.verifyWorkflow.exists();
  //   getDevicesWorkflowRunner.verifyWorkflow.hasNoDuplicates({});
  // });

  // 3. Should successfully onboard http requests device.
  it("3. Should successfully onboard http requests device.", function () {
    const importOnboardDevicesWorkflowWorkflow = false;
    const onboardDevicesWorkflowWorkflowStub = false;
    const workflowRunner = initializeWorkflowRunner(onboardDevicesWorkflow, importOnboardDevicesWorkflowWorkflow, onboardDevicesWorkflowWorkflowStub, []);
    // Onboard Device Workflow Stub
    const importOnboardDeviceWorkflow = true;
    const onboardDeviceWorkflowStub = true;
    const onboardDeviceWorkflowRunner = initializeWorkflowRunner(onboardDeviceWorkflow, importOnboardDeviceWorkflow, onboardDeviceWorkflowStub, httpOnboardingIAGGDDGetDeviceDetailsChildJobStub.stubTasks);
    // Inventory Device Onboarding Workflow Stub
    const importInventoryDeviceOnboardingWorkflow = true;
    const inventoryDeviceOnboardingWorkflowStub = true;
    const inventoryDeviceOnboardingWorkflowRunner = initializeWorkflowRunner(inventoryDeviceOnboardingWorkflow, importInventoryDeviceOnboardingWorkflow, inventoryDeviceOnboardingWorkflowStub, httpOnboardingCreateInventoryDeviceStub.stubTasks);

    const input = {
      "variables": {
        "device": [
          {
            "device_name": "F5_PBLT_645_http_Test",
            "inventory_type": "http_requests",
            "iag_adapter_instance": "DSUP IAG-2021.2",
            "onboarding": {
              "base_url": "172.20.100.91:443/mgmt",
              "protocol": "https",
              "endpoint": "/tm/ltm/pool",
              "method": "",
              "params": {},
              "data": {},
              "headers": {},
              "cookies": {},
              "auth": {
                "username": "admin",
                "password": "admin"
              },
              "timeout": {},
              "allowRedirects": true,
              "proxies": {},
              "verify": false
            }
          }
        ],
        "options": {
          "verbose": false
        }
      },
      "description": "3. Should successfully onboard http requests device."
    };

    workflowRunner.job.startAndReturnResultsWhenComplete({
      options: input,
      retryTime: 2000,
    }).then((jobVariableResults) => {

      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['message']).to.eql('Successfully onboarded device to IAG');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['status']).to.eql('SUCCESS');

      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_successfully_onboarded_devices']).to.eql(1);
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_success']).to.eql(1);
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_failure']).to.eql(0);
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['message']).to.eql('0 device(s) found already onboarded to IAG, 1 device(s) successfully onboarded to IAG, 0 device(s) failed to onboard to IAG');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['status']).to.eql('SUCCESS');
    });

    // Reimport orginal Onboard Device Workflow
    onboardDeviceWorkflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false,
    });
    onboardDeviceWorkflowRunner.importWorkflow.single({ workflow: onboardDeviceWorkflow });
    onboardDeviceWorkflowRunner.verifyWorkflow.exists();
    onboardDeviceWorkflowRunner.verifyWorkflow.hasNoDuplicates({});

    // Reimport orginal Inventory Device Onboarding Workflow
    inventoryDeviceOnboardingWorkflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false,
    });
    inventoryDeviceOnboardingWorkflowRunner.importWorkflow.single({ workflow: inventoryDeviceOnboardingWorkflow });
    inventoryDeviceOnboardingWorkflowRunner.verifyWorkflow.exists();
    inventoryDeviceOnboardingWorkflowRunner.verifyWorkflow.hasNoDuplicates({});
  });

  // // 4. Should fail onboarding existing http requests device.
  // it("4. Should fail onboarding existing http requests device.", function () {

  //   const importOnboardDevicesWorkflowWorkflow = false;
  //   const onboardDevicesWorkflowWorkflowStub = false;
  //   const workflowRunner = initializeWorkflowRunner(onboardDevicesWorkflow, importOnboardDevicesWorkflowWorkflow, onboardDevicesWorkflowWorkflowStub, []);
  //   const importgetDevicesWorkflow = true;
  //   const getDevicesWorkflowStub = true;
  //   const getDevicesWorkflowRunner = initializeWorkflowRunner(getDevicesWorkflow, importgetDevicesWorkflow, getDevicesWorkflowStub, getDeviceHTTPStub.stubTasks);

  //   const input = {
  //     "variables": {
  //       "device": [
  //         {
  //           "device_name": "F5_PBLT_645_http_Test",
  //           "inventory_type": "http_requests",
  //           "iag_adapter_instance": "DSUP IAG-2021.2",
  //           "onboarding": {
  //             "base_url": "172.20.100.91:443/mgmt",
  //             "protocol": "https",
  //             "endpoint": "/tm/ltm/pool",
  //             "method": "",
  //             "params": {},
  //             "data": {},
  //             "headers": {},
  //             "cookies": {},
  //             "auth": {
  //               "username": "admin",
  //               "password": "admin"
  //             },
  //             "timeout": {},
  //             "allowRedirects": true,
  //             "proxies": {},
  //             "verify": false
  //           }
  //         }
  //       ],
  //       "options": {
  //         "verbose": false
  //       }
  //     },
  //     "description": "4. Should fail onboarding existing http requests device."
  //   };

  //   workflowRunner.job.startAndReturnResultsWhenComplete({
  //     options: input,
  //     retryTime: 2000,
  //   }).then((jobVariableResults) => {

  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['message']).to.eql('Device already exists on IAG');
  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['status']).to.eql('SUCCESS');

  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_already_onboarded_devices']).to.eql(1);
  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_success']).to.eql(1);
  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_failure']).to.eql(0);
  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['message']).to.eql('1 device(s) found already onboarded to IAG, 0 device(s) successfully onboarded to IAG, 0 device(s) failed to onboard to IAG');
  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['status']).to.eql('SUCCESS');
  //   });

  //   getDevicesWorkflowRunner.deleteWorkflow.allCopies({
  //     failOnStatusCode: false,
  //   });
  //   getDevicesWorkflowRunner.importWorkflow.single({ workflow: getDevicesWorkflow });
  //   getDevicesWorkflowRunner.verifyWorkflow.exists();
  //   getDevicesWorkflowRunner.verifyWorkflow.hasNoDuplicates({});
  // });

  // // 5. Should fail to get device details for invalid inventory type.
  // it("5. Should fail to get device details for invalid inventory type.", function () {
  //   const workflowRunner = initializeWorkflowRunner(onboardDevicesWorkflow, false);

  //   const input = {
  //     "variables": {
  //       "device": [
  //         {
  //           "device_name": "invalid_inventory_type_test_device",
  //           "inventory_type": "invalid_inventory_type",
  //           "iag_adapter_instance": "DSUP IAG-2021.2",
  //           "onboarding": {
  //             "ipaddress": "172.20.103.91",
  //             "port": "22",
  //             "user": "admin",
  //             "password": "admin",
  //             "device-type": "network_cli",
  //             "ostype": "ios",
  //             "become": "true",
  //             "become_method": "enable",
  //             "become_pass": "admin"
  //           }
  //         }
  //       ],
  //       "options": {
  //         "verbose": false
  //       }
  //     },
  //     "description": "5. Should fail to get device details for invalid inventory type."
  //   };

  //   workflowRunner.job.startAndReturnResultsWhenComplete({
  //     options: input,
  //     retryTime: 2000,
  //   }).then((jobVariableResults) => {

  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['message']).to.eql('Error getting device details from DSUP IAG-2021.2');
  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['errors'][0]['message']).to.eql('Error getting device details from DSUP IAG-2021.2');
  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['errors'][0]['error_details']).to.eql('Invalid inventory type');
  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['status']).to.eql('FAILED');

  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_success']).to.eql(0);
  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_failure']).to.eql(1);
  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['message']).to.eql('0 device(s) found already onboarded to IAG, 0 device(s) successfully onboarded to IAG, 1 device(s) failed to onboard to IAG');
  //     expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['status']).to.eql('FAILED');
  //   });
  // });
})

describe('Test Using Sample Form Data', function () {
  let onboardDevicesWorkflowWithFormWorkflow;
  let getDevicesWorkflow;
  let onboardDeviceWorkflow;
  let ansibleDeviceOnboardingWorkflow;
  let inventoryDeviceOnboardingWorkflow;

  let formAnsibleOnboardingIAGGDDGetDeviceDetailsChildJobStub;
  let formAnsibleOnboardingIAGDOAnsibleDeviceStub;
  let formHttpOnboardingIAGGDDGetDeviceDetailsChildJobStub;
  let formHttpOnboardingCreateInventoryDeviceStub;

  before(function () {
    // Import Workflows
    cy.fixture('../../../bundles/workflows/IAGDO: Onboard With Form.json').then((data) => {
      onboardDevicesWorkflowWithFormWorkflow = data;
    });
    cy.fixture('../../../bundles/workflows/IAGGDD: Get Device Details.json').then((data) => {
      getDevicesWorkflow = data;
    });
    cy.fixture('../../../bundles/workflows/IAGDO: Onboard Device.json').then((data) => {
      onboardDeviceWorkflow = data;
    });
    cy.fixture('../../../bundles/workflows/IAGDO: Ansible Device Onboarding').then((data) => {
      ansibleDeviceOnboardingWorkflow = data;
    });
    cy.fixture('../../../bundles/workflows/IAGDO: Inventory Device Onboarding.json').then((data) => {
      inventoryDeviceOnboardingWorkflow = data;
    });

    /* Import Stubs */
    cy.fixture('stubs/form data/formAnsibleOnboardingIAGGDDGetDeviceDetailsChildJobStub.json').then((data) => {
      formAnsibleOnboardingIAGGDDGetDeviceDetailsChildJobStub = data;
    });
    cy.fixture('stubs/form data/formAnsibleOnboardingIAGDOAnsibleDeviceStub.json').then((data) => {
      formAnsibleOnboardingIAGDOAnsibleDeviceStub = data;
    });
    cy.fixture('stubs/form data/formHttpOnboardingIAGGDDGetDeviceDetailsChildJobStub.json').then((data) => {
      formHttpOnboardingIAGGDDGetDeviceDetailsChildJobStub = data;
    });
    cy.fixture('stubs/form data/formHttpOnboardingCreateInventoryDeviceStub.json').then((data) => {
      formHttpOnboardingCreateInventoryDeviceStub = data;
    });
  })

  // 6. Should successfully onboard ansible device with sample form input.
  it("6. Should successfully onboard ansible device with sample form input.", function () {
    const importonboardDevicesWorkflowWithFormWorkflowData = false;
    const onboardDevicesWorkflowWithFormWorkflowDataStub = false;
    const workflowRunner = initializeWorkflowRunner(onboardDevicesWorkflowWithFormWorkflow, importonboardDevicesWorkflowWithFormWorkflowData, onboardDevicesWorkflowWithFormWorkflowDataStub, []);
    // Onboard Device Workflow Stub
    const importOnboardDeviceWorkflow = true;
    const onboardDeviceWorkflowStub = true;
    const onboardDeviceWorkflowRunner = initializeWorkflowRunner(onboardDeviceWorkflow, importOnboardDeviceWorkflow, onboardDeviceWorkflowStub, formAnsibleOnboardingIAGGDDGetDeviceDetailsChildJobStub.stubTasks);
    // Form Ansible Device Onboarding Workflow Stub
    const importAnsibleDeviceOnboardingWorkflow = true;
    const formAnsibleDeviceOnboardingWorkflowStub = true;
    const ansibleDeviceOnboardingWorkflowRunner = initializeWorkflowRunner(ansibleDeviceOnboardingWorkflow, importAnsibleDeviceOnboardingWorkflow, formAnsibleDeviceOnboardingWorkflowStub, formAnsibleOnboardingIAGDOAnsibleDeviceStub.stubTasks);

    const input = {
      "variables": {
        "formData": {
          "deviceName": "PBLT_645_ansible_Test2",
          "inventoryType": "ansible",
          "iagAdapterInstance": "DSUP IAG-2021.2",
          "ansible": {
            "ipaddress": "172.20.103.91",
            "deviceType": "network_cli",
            "port": "22",
            "ostype": "ios",
            "become": "true",
            "becomeMethod": "enable",
            "user": "admin"
          }
        }
      },
      "description": "6. Should successfully onboard ansible device with sample form input."
    };

    workflowRunner.job.startAndReturnResultsWhenComplete({
      options: input,
      retryTime: 2000,
    }).then((jobVariableResults) => {

      expect(jobVariableResults['tasks']['feaf']['variables']['outgoing']['value']['details'][0]['message']).to.eql('Successfully onboarded device to IAG');

      expect(jobVariableResults['tasks']['feaf']['variables']['outgoing']['value']['details'][0]['status']).to.eql('SUCCESS');

      expect(jobVariableResults['tasks']['feaf']['variables']['outgoing']['value']['number_successfully_onboarded_devices']).to.eql(1);
      expect(jobVariableResults['tasks']['feaf']['variables']['outgoing']['value']['number_success']).to.eql(1);
      expect(jobVariableResults['tasks']['feaf']['variables']['outgoing']['value']['number_failure']).to.eql(0);
      expect(jobVariableResults['tasks']['feaf']['variables']['outgoing']['value']['message']).to.eql('0 device(s) found already onboarded to IAG, 1 device(s) successfully onboarded to IAG, 0 device(s) failed to onboard to IAG');
      expect(jobVariableResults['tasks']['feaf']['variables']['outgoing']['value']['status']).to.eql('SUCCESS');
    });

    // Reimport orginal Onboard Device Workflow
    onboardDeviceWorkflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false,
    });
    onboardDeviceWorkflowRunner.importWorkflow.single({ workflow: onboardDeviceWorkflow });
    onboardDeviceWorkflowRunner.verifyWorkflow.exists();
    onboardDeviceWorkflowRunner.verifyWorkflow.hasNoDuplicates({});

    // Reimport orginal Ansible Device Onboarding Workflow
    ansibleDeviceOnboardingWorkflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false,
    });
    ansibleDeviceOnboardingWorkflowRunner.importWorkflow.single({ workflow: ansibleDeviceOnboardingWorkflow });
    ansibleDeviceOnboardingWorkflowRunner.verifyWorkflow.exists();
    ansibleDeviceOnboardingWorkflowRunner.verifyWorkflow.hasNoDuplicates({});
  });

  // 7. Should successfully onboard http requests device with sample form input.
  it("7. Should successfully onboard http requests device with sample form input.", function () {
    const importonboardDevicesWorkflowWithFormWorkflowData = false;
    const onboardDevicesWorkflowWithFormWorkflowDataStub = false;
    const workflowRunner = initializeWorkflowRunner(onboardDevicesWorkflowWithFormWorkflow, importonboardDevicesWorkflowWithFormWorkflowData, onboardDevicesWorkflowWithFormWorkflowDataStub, []);
    // Onboard Device Workflow Stub
    const importOnboardDeviceWorkflow = true;
    const onboardDeviceWorkflowStub = true;
    const onboardDeviceWorkflowRunner = initializeWorkflowRunner(onboardDeviceWorkflow, importOnboardDeviceWorkflow, onboardDeviceWorkflowStub, formHttpOnboardingIAGGDDGetDeviceDetailsChildJobStub.stubTasks);
    // Inventory Device Onboarding Workflow Stub
    const importInventoryDeviceOnboardingWorkflow = true;
    const formInventoryDeviceOnboardingWorkflowStub = true;
    const inventoryDeviceOnboardingWorkflowRunner = initializeWorkflowRunner(inventoryDeviceOnboardingWorkflow, importInventoryDeviceOnboardingWorkflow, formInventoryDeviceOnboardingWorkflowStub, formHttpOnboardingCreateInventoryDeviceStub.stubTasks);

    const input = {
      "variables": {
        "formData": {
          "deviceName": "F5_PBLT_645_http_Test2",
          "inventoryType": "http_requests",
          "iagAdapterInstance": "DSUP IAG-2021.2",
          "ansible": {},
          "httpRequests": {
            "baseUrl": "172.20.100.91:443/mgmt",
            "protocol": "https",
            "endpoint": "/tm/ltm/pool",
            "params": "{           \"partition\": \"Common\",           \"name\": \"testing-me-vs\",           \"destination\": \"11.1.1.11:443\",           \"pool\": \"test-me\",           \"description\": \"testing\"         }",
            "data": "{           \"partition\": \"Common\",           \"name\": \"testing-me-vs\"         }",
            "headers": "{           \"partition\": \"Common\",           \"name\": \"testing-me-vs\"         }",
            "auth": "{           \"username\": \"admin\",           \"password\": \"admin\"         }"
          }
        }
      },
      "description": "7. Should successfully onboard http requests device with sample form input."
    };

    workflowRunner.job.startAndReturnResultsWhenComplete({
      options: input,
      retryTime: 2000,
    }).then((jobVariableResults) => {

      expect(jobVariableResults['tasks']['feaf']['variables']['outgoing']['value']['details'][0]['message']).to.eql('Successfully onboarded device to IAG');
      expect(jobVariableResults['tasks']['feaf']['variables']['outgoing']['value']['details'][0]['status']).to.eql('SUCCESS');

      // Additional check for correctly formated object
      expect(jobVariableResults['tasks']['feaf']['variables']['outgoing']['value']['details'][0]['response']['add_device']['response']['variables']['params']).to.eql({
        "partition": "Common",
        "name": "testing-me-vs",
        "destination": "11.1.1.11:443",
        "pool": "test-me",
        "description": "testing"
      });

      expect(jobVariableResults['tasks']['feaf']['variables']['outgoing']['value']['number_successfully_onboarded_devices']).to.eql(1);
      expect(jobVariableResults['tasks']['feaf']['variables']['outgoing']['value']['number_success']).to.eql(1);
      expect(jobVariableResults['tasks']['feaf']['variables']['outgoing']['value']['number_failure']).to.eql(0);
      expect(jobVariableResults['tasks']['feaf']['variables']['outgoing']['value']['message']).to.eql('0 device(s) found already onboarded to IAG, 1 device(s) successfully onboarded to IAG, 0 device(s) failed to onboard to IAG');
      expect(jobVariableResults['tasks']['feaf']['variables']['outgoing']['value']['status']).to.eql('SUCCESS');
    });

    // Reimport orginal Onboard Device Workflow
    onboardDeviceWorkflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false,
    });
    onboardDeviceWorkflowRunner.importWorkflow.single({ workflow: onboardDeviceWorkflow });
    onboardDeviceWorkflowRunner.verifyWorkflow.exists();
    onboardDeviceWorkflowRunner.verifyWorkflow.hasNoDuplicates({});

    // Reimport orginal Inventory Device Onboarding Workflow
    inventoryDeviceOnboardingWorkflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false,
    });
    inventoryDeviceOnboardingWorkflowRunner.importWorkflow.single({ workflow: inventoryDeviceOnboardingWorkflow });
    inventoryDeviceOnboardingWorkflowRunner.verifyWorkflow.exists();
    inventoryDeviceOnboardingWorkflowRunner.verifyWorkflow.hasNoDuplicates({});
  });
})

describe('Test Adding List of Devices', function () {
  let onboardDevicesWorkflow;

  let listIAGDOOnboardDevicesStub;
  let listExistingIAGDOOnboardDevicesStub;
  let listInvalidIAGDOOnboardDevicesStub;

  before(function () {
    // Import Workflows
    cy.fixture(`../../../bundles/workflows/IAGDO: Onboard Devices.json`).then((data) => {
      onboardDevicesWorkflow = data;
    });

    /* Import Stubs */
    cy.fixture('stubs/list of devices/listIAGDOOnboardDevicesStub.json').then((data) => {
      listIAGDOOnboardDevicesStub = data;
    });
    cy.fixture('stubs/list of devices/listExistingIAGDOOnboardDevicesStub.json').then((data) => {
      listExistingIAGDOOnboardDevicesStub = data;
    });
    cy.fixture('stubs/list of devices/listInvalidIAGDOOnboardDevicesStub.json').then((data) => {
      listInvalidIAGDOOnboardDevicesStub = data;
    });
  })

  // 8. Should successfully onboard a list of devices - 1 ansible and 1 httprequests device.
  it("8. Should successfully onboard a list of devices - 1 ansible and 1 httprequests device.", function () {
    const importOnboardDevicesWorkflowWorkflow = true;
    const onboardDevicesWorkflowWorkflowStub = true;
    const workflowRunner = initializeWorkflowRunner(onboardDevicesWorkflow, importOnboardDevicesWorkflowWorkflow, onboardDevicesWorkflowWorkflowStub, listIAGDOOnboardDevicesStub.stubTasks);

    const input = {
      "variables": {
        "device": [
          {
            "device_name": "PBLT_645_ansible_Test3",
            "inventory_type": "ansible",
            "iag_adapter_instance": "DSUP IAG-2021.2",
            "onboarding": {
              "ipaddress": "172.20.103.91",
              "port": "22",
              "user": "admin",
              "password": "admin",
              "device-type": "network_cli",
              "ostype": "ios",
              "become": "true",
              "become_method": "enable",
              "become_pass": "admin"
            }
          },
          {
            "device_name": "F5_PBLT_645_http_Test3",
            "inventory_type": "http_requests",
            "iag_adapter_instance": "DSUP IAG-2021.2",
            "onboarding": {
              "base_url": "172.20.100.91:443/mgmt",
              "protocol": "https",
              "endpoint": "/tm/ltm/pool",
              "method": "",
              "params": {},
              "data": {},
              "headers": {},
              "cookies": {},
              "auth": {
                "username": "admin",
                "password": "admin"
              },
              "timeout": {},
              "allowRedirects": true,
              "proxies": {},
              "verify": false
            }
          }
        ],
        "options": {
          "verbose": false
        }
      },
      "description": "8. Should successfully onboard a list of devices - 1 ansible and 1 httprequests device."
    };

    workflowRunner.job.startAndReturnResultsWhenComplete({
      options: input,
      retryTime: 2000,
    }).then((jobVariableResults) => {

      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['message']).to.eql('Successfully onboarded device to IAG');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['status']).to.eql('SUCCESS');

      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][1]['message']).to.eql('Successfully onboarded device to IAG');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][1]['status']).to.eql('SUCCESS');

      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_successfully_onboarded_devices']).to.eql(2);
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_success']).to.eql(2);
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_failure']).to.eql(0);
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['message']).to.eql('0 device(s) found already onboarded to IAG, 2 device(s) successfully onboarded to IAG, 0 device(s) failed to onboard to IAG');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['status']).to.eql('SUCCESS');
    });

    // Reimport orginal Onboard Device Workflow
    workflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false,
    });
    workflowRunner.importWorkflow.single({ workflow: onboardDevicesWorkflow });
    workflowRunner.verifyWorkflow.exists();
    workflowRunner.verifyWorkflow.hasNoDuplicates({});
  });

  // 9. Should fail onboarding a list of existing devices - 1 ansible and 1 httprequests device.
  it("9. Should fail onboarding a list of existing devices - 1 ansible and 1 httprequests device.", function () {
    const importOnboardDevicesWorkflowWorkflow = true;
    const onboardDevicesWorkflowWorkflowStub = true;
    const workflowRunner = initializeWorkflowRunner(onboardDevicesWorkflow, importOnboardDevicesWorkflowWorkflow, onboardDevicesWorkflowWorkflowStub, listExistingIAGDOOnboardDevicesStub.stubTasks);

    const input = {
      "variables": {
        "device": [
          {
            "device_name": "PBLT_645_ansible_Test3",
            "inventory_type": "ansible",
            "iag_adapter_instance": "DSUP IAG-2021.2",
            "onboarding": {
              "ipaddress": "172.20.103.91",
              "port": "22",
              "user": "admin",
              "password": "admin",
              "device-type": "network_cli",
              "ostype": "ios",
              "become": "true",
              "become_method": "enable",
              "become_pass": "admin"
            }
          },
          {
            "device_name": "F5_PBLT_645_http_Test3",
            "inventory_type": "http_requests",
            "iag_adapter_instance": "DSUP IAG-2021.2",
            "onboarding": {
              "base_url": "172.20.100.91:443/mgmt",
              "protocol": "https",
              "endpoint": "/tm/ltm/pool",
              "method": "",
              "params": {},
              "data": {},
              "headers": {},
              "cookies": {},
              "auth": {
                "username": "admin",
                "password": "admin"
              },
              "timeout": {},
              "allowRedirects": true,
              "proxies": {},
              "verify": false
            }
          }
        ],
        "options": {
          "verbose": false
        }
      },
      "description": "9. Should fail onboarding a list of existing devices - 1 ansible and 1 httprequests device."
    };

    workflowRunner.job.startAndReturnResultsWhenComplete({
      options: input,
      retryTime: 2000,
    }).then((jobVariableResults) => {

      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['message']).to.eql('Device already exists on IAG');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['status']).to.eql('SUCCESS');

      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][1]['message']).to.eql('Device already exists on IAG');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][1]['status']).to.eql('SUCCESS');

      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_already_onboarded_devices']).to.eql(2);
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_success']).to.eql(2);
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_failure']).to.eql(0);
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['message']).to.eql('2 device(s) found already onboarded to IAG, 0 device(s) successfully onboarded to IAG, 0 device(s) failed to onboard to IAG');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['status']).to.eql('SUCCESS');
    });

    // Reimport orginal Onboard Device Workflow
    workflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false,
    });
    workflowRunner.importWorkflow.single({ workflow: onboardDevicesWorkflow });
    workflowRunner.verifyWorkflow.exists();
    workflowRunner.verifyWorkflow.hasNoDuplicates({});
  });

  // 10. Should fail onboarding a list of devices due to invalid inventory type - 1 ansible and 1 httprequests device.
  it("10. Should fail onboarding a list of devices due to invalid inventory type - 1 ansible and 1 httprequests device.", function () {
    const importOnboardDevicesWorkflowWorkflow = true;
    const onboardDevicesWorkflowWorkflowStub = true;
    const workflowRunner = initializeWorkflowRunner(onboardDevicesWorkflow, importOnboardDevicesWorkflowWorkflow, onboardDevicesWorkflowWorkflowStub, listInvalidIAGDOOnboardDevicesStub.stubTasks);

    const input = {
      "variables": {
        "device": [
          {
            "device_name": "invalid_inventory_type_test_device3",
            "inventory_type": "invalid_inventory_type1",
            "iag_adapter_instance": "DSUP IAG-2021.2",
            "onboarding": {
              "ipaddress": "172.20.103.91",
              "port": "22",
              "user": "admin",
              "password": "admin",
              "device-type": "network_cli",
              "ostype": "ios",
              "become": "true",
              "become_method": "enable",
              "become_pass": "admin"
            }
          },
          {
            "device_name": "invalid_inventory_type_test_device3",
            "inventory_type": "invalid_inventory_type2",
            "iag_adapter_instance": "DSUP IAG-2021.2",
            "onboarding": {
              "base_url": "172.20.100.91:443/mgmt",
              "protocol": "https",
              "endpoint": "/tm/ltm/pool",
              "method": "",
              "params": {},
              "data": {},
              "headers": {},
              "cookies": {},
              "auth": {
                "username": "admin",
                "password": "admin"
              },
              "timeout": {},
              "allowRedirects": true,
              "proxies": {},
              "verify": false
            }
          }
        ],
        "options": {
          "verbose": false
        }
      },
      "description": "10. Should fail onboarding a list of devices due to invalid inventory type - 1 ansible and 1 httprequests device."
    };

    workflowRunner.job.startAndReturnResultsWhenComplete({
      options: input,
      retryTime: 2000,
    }).then((jobVariableResults) => {

      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['message']).to.eql('Error getting device details from DSUP IAG-2021.2');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['errors'][0]['message']).to.eql('Error getting device details from DSUP IAG-2021.2');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['errors'][0]['error_details']).to.eql('Invalid inventory type');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][0]['status']).to.eql('FAILED');

      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][1]['message']).to.eql('Error getting device details from DSUP IAG-2021.2');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][1]['errors'][0]['message']).to.eql('Error getting device details from DSUP IAG-2021.2');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][1]['errors'][0]['error_details']).to.eql('Invalid inventory type');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['details'][1]['status']).to.eql('FAILED');

      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_success']).to.eql(0);
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['number_failure']).to.eql(2);
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['message']).to.eql('0 device(s) found already onboarded to IAG, 0 device(s) successfully onboarded to IAG, 2 device(s) failed to onboard to IAG');
      expect(jobVariableResults['tasks']['20a2']['variables']['outgoing']['ReturnStatus']['status']).to.eql('FAILED');
    });

    // Reimport orginal Onboard Devices Workflow
    workflowRunner.deleteWorkflow.allCopies({
      failOnStatusCode: false,
    });
    workflowRunner.importWorkflow.single({ workflow: onboardDevicesWorkflow });
    workflowRunner.verifyWorkflow.exists();
    workflowRunner.verifyWorkflow.hasNoDuplicates({});
  });
})