
## 1.0.5 [06-21-2023]

* Merging pre-release/2023.1 into master to run cypress tests

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!33

---

## 1.0.4 [05-19-2023]

* Updates version for npmjs

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!31

---

## 1.0.2 [05-19-2023]

* Updates README and cypress dependency.

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!30

---

## 1.0.1 [04-24-2023]

* Patch/pblt 1090 20221

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!24

---

## 1.1.0-2021.2.0 [02-17-2023]

* Merge master into release/2021.2

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!21

---

## 1.1.0 [02-16-2023]

* Update README.md

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!9

---

## 0.0.7 [06-20-2022]

* Update README.md

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!9
## 0.0.6-2021.2.0 [01-05-2022]

* 21.2 certification

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!8

---

## 0.0.6 [01-05-2022]

* 21.2 certification

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!8

---

## 0.0.5 [11-15-2021]

* Patch/dsup 1004

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!7

---

## 0.0.4 [07-02-2021]

* Update package.json

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!6

---

## 0.0.3 [06-11-2021]

* Certify on IAP 2021.1

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!5

---

## 0.0.2 [02-09-2021]

* Patch/lb 515

See merge request itentialopensource/pre-built-automations/iag-device-onboarding!4

---

## 0.0.4 [07-17-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.3 [07-07-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---

## 0.0.2 [06-19-2020]

* [patch/LB-404] Update readme template to follow standard

See merge request itentialopensource/pre-built-automations/artifact-template-2020.1!1

---\n\n\n\n\n
